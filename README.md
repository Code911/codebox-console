## Overview ##

Console addon for Codebox, which displays terminal tab connected to opened VM
instance.

## Install ##

To install addon, you must have codebox setup with core addons installed

    git clone git@bitbucket.org:Code911/codebox.git && cd codebox
    npm install
    npm install -g gulp jsctags
    export NODE_PATH=$NODE_PATH/jsctags/jsctags:$NODE_PATH
    gulp
    mkdir -p ~/.codebox/packages
    node ./bin/codebox.js install

In the command below, replace `<bitbucket user>` with actual bitbucket login.

        node ./bin/codebox.js install -p 'console:https://<bitbucket user>@bitbucket.org/Code911/codebox-console.git'

Finally, run codebox. Specifiy folder for workspace root.

        node ./bin/codebox.js run /some/folder

## Addon setup ##

Addon connects to the instance, using command `ssh instance`. To specify
connection parameters, it is necessary to configure ssh connection in file
`~/.ssh/config`

    Host instance
        HostName 52.28.43.125
        Port 22
        User ubuntu




