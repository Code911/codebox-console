'use strict';

var path = require('path'),
    term = require('./term'),
    fs = require('fs'),
    pty = require('pty.js');


module.exports = function(codebox) {
    var events = codebox.events;
    console.log("Current Env", process.env);
    console.log("Current settings", codebox.settings);
    
    var command = process.env['CONSOLE_CMD'] || 'bash';
    var args = [];
    if (process.env['CONSOLE_ARGS']) {
        args = process.env['CONSOLE_ARGS'].split(' '); 
    }
    console.log("Command", command, " Args ", args);
    
    codebox.socket.service("simulator", function(socket) {
        var term = pty.fork(command, args, {
            name: require('fs').existsSync('/usr/share/terminfo/x/xterm-256color')
            ? 'xterm-256color'
            : 'xterm',
            cols: 80,
            rows: 24,
            cwd: process.env.HOME
        });
        
        term.on('data', function(data) {
            socket.write(JSON.stringify({
                "data":data
            }));
        });
        
        socket.on('data', function(data) {
            var message, i = 0;
            try {
                message = JSON.parse(data);
                if (message.resize){
                    term.resize( message.resize.col, message.resize.row);
                }
                else if (message.data) {
                    term.write(message.data);
                }
            } catch (err) {
                console.error(err);
            } 
        });
        
        socket.on('close', function() {
            if (term) term.end();
            term = null;
            socket = null;
        });
        
        socket.on('resize', function(data) {
            if (term) {
                term.resize(data.col, data.row);
            }
        });
    
        term.on('exit', function() {
            if (socket) socket.end();
            socket = null;
            term = null;
        });


    });
};
