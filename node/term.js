'use strict';

var fs = require('fs'),
    path = require('path'),
    pty = require('pty.js');

var settingsFile = path.join(process.env['HOME'], '.codebox/simulator-config.json'),
    command = {
        name: 'python',
        args: []
    };

/**
 * Optionally read the configuration from ~/.codebox/simulator-config.json
 * The file should contain command line options to connect to simulator.
 * File format is:
 * {
     "command": "command-name",
     "args": ["list", "of", "arguments"]
    }
**/
fs.exists(settingsFile, function(exists) {
    var data;
    if (!exists) {
        return;
    }
    data = fs.readFileSync(settingsFile);
    try {
        data = JSON.parse(data);
    } catch (e) {
        console.log(e);
        data = {};
    } finally {
        if (typeof(data.command) === 'string' && Array.isArray(data.args)) {
            command = data;
        }
    }
});

function socketService(socket) {
    var term = pty.fork(command.command, command.args, {
        name: require('fs').existsSync('/usr/share/terminfo/x/xterm-256color')
        ? 'xterm-256color'
        : 'xterm',
        cols: 80,
        rows: 24,
        cwd: process.env.HOME
    });
    
    term.on('data', function(data) {
        socket.write(JSON.stringify({
            "data":data
        }));
    });
    
    socket.on('data', function(data) {
        var message, i = 0;
        try {
            message = JSON.parse(data);
            data = message.data;
        } catch (err) {
            data = '';
        } finally {
            term.write(data);
        }
    });
    
    socket.on('close', function() {
        if (term) term.end();
        term = null;
        socket = null;
    });
    
    term.on('exit', function() {
        if (socket) socket.end();
        socket = null;
        term = null;
    });

}

module.exports = {
    "socketService": socketService
};
