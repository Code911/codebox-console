require("./stylesheets/xterm.css");
var _ = codebox.require("hr.utils");
var commands = codebox.require("core/commands"),
    Console = require("./views/console");
require("./stylesheets/main.less");

codebox.app.once("ready", function() {

   var temp = codebox.settings.exportJson();
   var obj = [{"command": "console.open"}];             
   _.extend(temp.tree.toolbar, obj);
   codebox.settings.importJSON(temp);
  
}),


commands.register({
    id: "console.open",
    title: "Console: Open",
    icon: "terminal",
    run: function(args, context) {
        return codebox.tabs.add(Console, args, {
            type: "terminal",
            title: "Console"
        });
    }
});

