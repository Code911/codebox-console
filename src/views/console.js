var $ = codebox.require("jquery");
var _ = codebox.require("hr.utils");
var Socket = codebox.require("core/socket");
var hterm = require("./hterm_all.js").hterm;
var lib = require("./hterm_all.js").lib;

var menu = codebox.require("utils/menu");
var dialogs = codebox.require("utils/dialogs");

function Wetty(argv) {
    this.argv_ = argv;
    this.io = null;
    this.pid_ = -1;
}

Wetty.prototype.run = function() {
    var that = this;
    this.io = this.argv_.io.push();
    
    socket = new Socket({
                service: "simulator"
    });
    this.socket = socket;
    this.socket.once('open', function() {
          that.io.onVTKeystroke = that.sendString_.bind(that);
          that.io.sendString = that.sendString_.bind(that);
          that.io.onTerminalResize = that.onTerminalResize.bind(that);
          that.onTerminalResize(that.io.columnCount, that.io.rowCount);
          
          that.socket.on("data", function(d) {
                if (d.data) {
                    that.io.writeUTF16(d.data);
                }
          });
            
    });

}

Wetty.prototype.sendString_ = function(str) {
    this.socket.sock.send(JSON.stringify({"data":str}));
};

Wetty.prototype.onTerminalResize = function(col, row) {
    this.socket.sock.send(JSON.stringify({'resize': { col: col, row: row }}));
};



var ConsoleView = codebox.tabs.Panel.extend({
    className: "console",
    
    initialize: function(options) {
        ConsoleView.__super__.initialize.apply(this, arguments);
        // Context menu
        var that = this;

        lib.init(function() {
        hterm.defaultStorage = new lib.Storage.Local();
            var term = new hterm.Terminal();
            that.term = term;
            term.decorate(that.$el[0]);
    
            term.setCursorPosition(0, 0);
            term.setCursorVisible(true);
            term.prefs_.set('ctrl-c-copy', true);
            term.prefs_.set('ctrl-v-paste', true);
            term.prefs_.set('use-default-window-copy', true);
            term.runCommandClass(Wetty, document.location.hash.substr(1));  
            
        });
        menu.add(this.$el, this.getContextMenu.bind(this));
    },

    
    render: function() {
        
        this.resizeHandler = _.bind(this.resize, this);
            
        $(window).resize(this.resizeHandler);
        this.on("tab:layout", this.resizeHandler);
        
        return ConsoleView.__super__.render.apply(this, arguments);
    },
    
    finish: function() {
            
        return ConsoleView.__super__.finish.apply(this, arguments);
    },

    remove: function() {
        $(window).unbind('resize', this.resizeHandler);
        this.off("tab:layout", this.resizeHandler);
        
        if (this.term) {
            //this.term.Frame.close();
            this.term = null;
        }

        return ConsoleView.__super__.remove.apply(this, arguments);
    },

    // Generate the context menu items
    getContextMenu: function() {
        var that = this;
        var items = [
            {
                label: "Copy    (Ctrl+C)",
                click: function() {
                    return that.term.document.execCommand('copy');                    
                }
            },
            {
                label: "Paste   (Ctrl+V)",
                click: function() {
                    
                    that.term.handler(window.clipboard);
                    that.term.textarea.value = '';
                    return that.term.focus();

                }
            }            
        ];

        return items;
    } ,
    
    resize: function() {
        var that = this;
        // We need to create a new terminal as older one is lost
        lib.init(function() {
        hterm.defaultStorage = new lib.Storage.Local();
            var term = new hterm.Terminal();
            that.term = term;
            term.decorate(that.$el[0]);
            term.setCursorPosition(0, 0);
            term.setCursorVisible(true);
            term.prefs_.set('ctrl-c-copy', true);
            term.prefs_.set('ctrl-v-paste', true);
            term.prefs_.set('use-default-window-copy', true);
            term.runCommandClass(Wetty, document.location.hash.substr(1));  
            
        });
                    
        return this;
    }
    
});

module.exports = ConsoleView;

